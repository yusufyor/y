﻿using AdminPanel.Data.Entity;
using MySql.Data.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminPanel.Data.Context
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class APContext : DbContext
    {
        public virtual DbSet<MediaEntity> Medias { get; set; }
        public virtual DbSet<ProductEntity> Products { get; set; }
        public virtual DbSet<ProductCategoryEntity> ProductCategories { get; set; }
        public virtual DbSet<UserEntity> Users { get; set; }
        public virtual DbSet<LogEntity> Logs { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //base.OnModelCreating(modelBuilder);

            //modelBuilder.Entity<ProductEntity>().ToTable("Products");
            //modelBuilder.Entity<ProductCategoryEntity>().ToTable("ProductCategories");

            //  modelBuilder.Entity<ProductEntity>()
            //.HasRequired<ProductCategoryEntity>(b => b.Category)
            //.WithMany(a => a.Products)
            //.HasForeignKey<long>(b => b.CategoryId);

        }


        public APContext() : base("DefaultConnection")
        {
            //this.Configuration.ValidateOnSaveEnabled = false;
            //this.Configuration.LazyLoadingEnabled = true;
            //this.Configuration.ProxyCreationEnabled = true;
        }
    }
}
