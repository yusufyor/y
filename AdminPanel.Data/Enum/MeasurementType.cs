﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminPanel.Data.Enum
{
    public enum MeasurementType
    {
        [Display(Name = "Kg")]
        KG = 1,
        [Display(Name = "Adet")]
        AMOUNT = 2
    }
}
