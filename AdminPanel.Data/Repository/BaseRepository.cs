﻿using AdminPanel.Data.Context;
using AdminPanel.Data.Entity;
using AdminPanel.Data.Enum;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AdminPanel.Data.Repository
{

    public class BaseRepository<TEntity, TContext> : IRepository<TEntity>
        where TEntity : BaseEntity, new()
        where TContext : APContext, new()
    {
        private TContext Context;
        public void InitContext(TContext _context)
        {
            this.Context = _context;
        }

        public BaseRepository()
        {
            Context = new TContext();
        }

        public virtual List<TEntity> Get(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "")
        {

            var dbset = Context.Set<TEntity>();

            IQueryable<TEntity> query;

            //var fdsf = dbset.ToList<TEntity>();

            if (filter != null)
            {
                query = dbset.Where(filter);
            }
            else
                query = dbset;

            foreach (var includeProperty in includeProperties.Split
            (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                var data = orderBy(query).ToList<TEntity>();
                return data;
            }
            else
            {
                var data = query.ToList();
                return data;
            }
        }

        public virtual TEntity GetById(long Id, string includeProperties = "")
        {

            TEntity entity = new TEntity();
            IQueryable<TEntity> query = Context.Set<TEntity>();

            foreach (var includeProperty in includeProperties.Split
                    (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            entity = query.Where(x => x.Id == Id).FirstOrDefault();

            return entity;
        }

        public virtual bool Insert(TEntity entity)
        {

            Context.Set<TEntity>().Add(entity);
            return Context.SaveChanges() > 0 ? true : false;
        }

        public virtual bool Delete(object id)
        {
            TEntity entityToDelete = Context.Set<TEntity>().Find(id);
            entityToDelete.Status = (int)StatusType.Deleted;
            return Update(entityToDelete);
        }

        public virtual bool Delete(TEntity entityToDelete)
        {
            entityToDelete.Status = (int)StatusType.Deleted;
            return Context.SaveChanges() > 0 ? true : false;
        }

        public virtual bool Update(TEntity entityToUpdate)
        {
            var addedEntity = Context.Entry(entityToUpdate);


            var local = Context.Set<TEntity>()
                         .Local
                         .FirstOrDefault(f => f.Id == entityToUpdate.Id);
            if (local != null)
            {
                Context.Entry(local).State = EntityState.Detached;
            }

            addedEntity.State = EntityState.Modified;
            return Context.SaveChanges() > 0 ? true : false;
        }

    }
}
