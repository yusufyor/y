﻿using AdminPanel.Data.Context;
using AdminPanel.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminPanel.Data.Repository
{
    public interface IProductCategoryRepository : IRepository<ProductCategoryEntity>
    { }

    public class ProductCategoryRepository : BaseRepository<ProductCategoryEntity, APContext>, IProductCategoryRepository
    {
        private readonly APContext ApCntxt;

        public ProductCategoryRepository()
        {
            ApCntxt = new APContext();
            base.InitContext(ApCntxt);
        }
    }
}
