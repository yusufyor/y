﻿using AdminPanel.Data.Context;
using AdminPanel.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminPanel.Data.Repository
{
    public interface IProductRepository : IRepository<ProductEntity>
    {
    }

    public class ProductRepository : BaseRepository<ProductEntity, APContext>, IProductRepository
    {
        private readonly APContext ApCntxt;
        public ProductRepository()
        {
            ApCntxt = new APContext();
            base.InitContext(ApCntxt);
        }
    }
}
