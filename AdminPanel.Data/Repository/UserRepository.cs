﻿using AdminPanel.Data.Context;
using AdminPanel.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminPanel.Data.Repository
{
    public interface IUserRepository : IRepository<UserEntity>
    {
    }
    public class UserRepository : BaseRepository<UserEntity, APContext>, IUserRepository
    {
        private readonly APContext ApCntxt;
        public UserRepository()
        {
            ApCntxt = new APContext();
            base.InitContext(ApCntxt);
        }
    }
}