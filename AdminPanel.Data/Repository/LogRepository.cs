﻿using AdminPanel.Data.Context;
using AdminPanel.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminPanel.Data.Repository
{
    public interface ILogRepository : IRepository<LogEntity>
    { }
    public class LogRepository : BaseRepository<LogEntity, APContext>, ILogRepository
    {
        private readonly APContext ApCntxt;

        public LogRepository()
        {
            ApCntxt = new APContext();
            base.InitContext(ApCntxt);
        }
    }
}
