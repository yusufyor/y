﻿using AdminPanel.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AdminPanel.Data.Repository
{
    public interface IRepository<TEntity> where TEntity : BaseEntity
    {
        bool Delete(TEntity entityToDelete);
        bool Delete(object id);
        List<TEntity> Get(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "");
        TEntity GetById(long Id, string includeProperties = "");
   
        bool Insert(TEntity entity);
        bool Update(TEntity entityToUpdate);
    }
}
