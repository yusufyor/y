﻿using AdminPanel.Data.Context;
using AdminPanel.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminPanel.Data.Repository
{
    public interface IMediaRepository : IRepository<MediaEntity>
    {
    }

    public class MediaRepository : BaseRepository<MediaEntity, APContext>, IMediaRepository
    {
        private readonly APContext ApCntxt;
        public MediaRepository()
        {
            ApCntxt = new APContext();
            base.InitContext(ApCntxt);
        }
    }
}
