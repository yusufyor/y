﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminPanel.Data.Entity
{
    public class BaseEntity
    {
        [Key]
        [Column("id")]
        public long Id { get; set; }

        [Column("status")]
        public int Status { get; set; }
    }
}
