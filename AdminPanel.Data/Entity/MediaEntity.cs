﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminPanel.Data.Entity
{
    [Table("media")]
    public class MediaEntity : BaseEntity
    {
        [Column("link")]
        public string Link { get; set; }

    }
}
