﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminPanel.Data.Entity
{
    [Table("user")]
    public class UserEntity : BaseEntity
    {
        [Column("username")]
        public string UserName { get; set; }

        [Column("first_name")]
        public string FirstName { get; set; }


        [Column("last_name")]
        public string LastName { get; set; }


        [Column("password")]
        public string Password { get; set; }
    }
}
