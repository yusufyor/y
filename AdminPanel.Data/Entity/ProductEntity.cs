﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminPanel.Data.Entity
{
    [Table("product")]
    public class ProductEntity : BaseEntity
    {
        [Column("name")]
        public string Name { get; set; }

        [Column("measurement_type")]
        public int MeasurementType { get; set; }

        [Column("price")]
        public decimal Price { get; set; }

        [Column("max_cart_amount")]
        public int? MaxCartAmount { get; set; }

   
        [Column("category_id")]
        public long CategoryId { get; set; }

        [ForeignKey("CategoryId")]
        public virtual ProductCategoryEntity Category { get; set; }

    }
}
