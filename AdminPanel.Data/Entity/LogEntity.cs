﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminPanel.Data.Entity
{
    [Table("log")]
    public class LogEntity : BaseEntity
    {
        [Column("table_name")]
        public string TableName { get; set; }

        [Column("pk_id")]
        public long PkId { get; set; }

        [Column("description")]
        public string Description { get; set; }

        [Column("user_id")]
        public long? UserId { get; set; }

        [Column("date")]
        public DateTime? Date { get; set; }
    }
}
