﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminPanel.Data.Entity
{
   [Table("product_category")]
    public class ProductCategoryEntity : BaseEntity
    {
        [Column("name")]
        public string Name { get; set; }

        [Column("media_id")]
        public long? MediaId { get; set; }

        [Column("parent_id")]
        public long? ParentId { get; set; }

        [ForeignKey("MediaId")]
        public virtual MediaEntity Media { get; set; }

        [ForeignKey("ParentId")]
        public virtual ProductCategoryEntity ParentProductCategory { get; set; }

        public virtual ICollection<ProductEntity> Products { get; set; }

    }
}
