﻿using AdminPanel.Data.Attr;
using AdminPanel.Data.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AdminPanel.Data.Model
{
    [Table("product")]
    public class ProductModel : BaseModel
    {
        [Required(ErrorMessage = "Ürün adı zorunludur")]
        [StringLength(100, ErrorMessage ="Ürün adı karakterden fazla olamaz")]
        [DataType(DataType.Text)]
        [Display(Name = "Ürün Adı")]
        [Log]
        public string Name { get; set; }


        [Required(ErrorMessage = "Birim seçmelisiniz")]
        public int MeasurementType { get; set; }

        [Required(ErrorMessage = "Birim girmelisiniz")]
        [Range(0, 2, ErrorMessage = "Birim seçiniz")]
        [Display(Name = "Birim")]
        [EnumDataType(typeof(MeasurementType),ErrorMessage ="Birim girmelisiniz")]
        public MeasurementType MeasurementTypeEnum
        {
            //get;set;
            get
            {
                return (MeasurementType)MeasurementType;
            }
            set
            {
                MeasurementType = (int)value;
            }
        }

        public string MeasurementTypeText
        {
            get
            {
                try
                {
                    return typeof(AdminPanel.Data.Enum.MeasurementType).GetMember(MeasurementTypeEnum.ToString())
                         .First()
                         .GetCustomAttribute<DisplayAttribute>()
                         .Name;
                }
                catch { return ""; }
            }
        }

        [Required(ErrorMessage = "Fiyat girmelisiniz")]
        [Display(Name = "Fiyat")]
        //[Range(0.01, 100000.00)]
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:#.#}")]
        public decimal Price { get; set; }

        [Display(Name = "Fiyat")]
        [Required(ErrorMessage = "Fiyat girmelisiniz")]
        //[Range(0.01, 100000.00)]
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:#.#}")]
        public string PriceText
        {
            get
            {
                return Price.ToString();
            }
            set
            {
                Price = decimal.Parse(value.Replace(".", ","));
            }
        }

        [Display(Name = "Max Sepet Adet")]
        [DataType(DataType.Text)]
        public int? MaxCartAmount { get; set; }

        [Required(ErrorMessage = "Kategori seçmelisiniz")]
        [Display(Name = "Kategori")]
        [DataType(DataType.Text)]
        public long CategoryId { get; set; }

        public ProductCategoryModel Category { get; set; }
    }
}
