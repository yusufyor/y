﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminPanel.Data.Model.ViewModel
{
    public class ProductVM
    {
        [Display(Name = "Kategori")]
        public List<ProductCategoryModel> ProductCategoryList { get; set; } = new List<ProductCategoryModel>();
        [Required(ErrorMessage = "Kategori seçmelisiniz")]
        public int SelectedProductCategoryId { get; set; }

        [Required(ErrorMessage = "Birim seçmelisiniz")]
        public int SelectedMeasureType { get; set; }
        public ProductModel ProductModel { get; set; } = new ProductModel();
    }
}
