﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminPanel.Data.Model.ViewModel
{
    public class ResultVM
    {
        public bool IsSuccess { get; set; }
        public List<string> ErrorMessage { get; set; }
    }
}
