﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AdminPanel.Data.Model.ViewModel
{
    public class CategoryVM
    {
        public ProductCategoryModel  ProductCategoryModel{ get; set; } = new ProductCategoryModel();

        //public HttpPostedFileBase file { get; set; } 
        public int SelectedParentProductCategoryId { get; set; }
        [Display(Name = "Kategori")]
        public List<ProductCategoryModel> ProductCategoryList { get; set; } = new List<ProductCategoryModel>();
    }
}
