﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminPanel.Data.Model
{
    public class ProductCategoryModel : BaseModel
    {
        [Required(ErrorMessage = "Kategori adı zorunludur")]
        [StringLength(100, ErrorMessage = "Kategori adı karakterden fazla olamaz")]
        [DataType(DataType.Text)]
        [Display(Name = "Kategori Adı")]
        public string Name { get; set; }

        public long? MediaId { get; set; }

        public long? ParentId { get; set; }

        public ProductCategoryModel ParentProductCategory { get; set; }


        public MediaModel Media { get; set; } = new MediaModel();
    }
}
