﻿using AdminPanel.Data.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AdminPanel.Data.Model
{
    public class MediaModel : BaseModel
    {
        public string Link { get; set; }

        public HttpPostedFileBase HttpPostedFileBase { get; set; }
        public MediaType MediaType { get; set; }
    }
}
