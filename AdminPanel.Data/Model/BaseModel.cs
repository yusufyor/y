﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AdminPanel.Data.Model
{
    public class BaseModel
    {
        [HiddenInput]
        public long Id { get; set; }

        [Display(Name = "Aktif ? ")]
        public int Status { get; set; }

        [Display(Name = "Aktif ? ")]
        public bool StatusFCheckBox
        {
            get { return Status == 0; }
            set
            {
                Status = value ? 0 : 1;
            }
        }
    }
}
