﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminPanel.Data.Model
{
    public class LogModel : BaseModel
    {
        public string TableName { get; set; }
        public long PkId { get; set; }
        public string Description { get; set; }
        public long? UserId { get; set; }
        public DateTime? Date { get; set; }
    }
}
