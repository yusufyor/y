function ConfirmViaFunc(title, text, type, funcName, parameters) {
    debugger;
    swal({
        title: title,
        text: text,
        type: type,
        showCancelButton: true,
        cancelButtonText: "HAYIR",
        confirmButtonText: "EVET",
        closeOnConfirm: true
    },
        function (isConfirm) {
       
            if (isConfirm) {
                debugger;
                var fn = window[funcName];
                if (typeof fn === "function") fn.apply(null, JSON.parse("[" + parameters + "]"));
            }
            else {
            }
        });
}

function Notify(title, text, icon) {
    swal({
        title: title,
        text: text,
        icon: icon
    });
}