﻿using AdminPanel.Business.Helper;
using AdminPanel.Business.Service;
using AdminPanel.Data.Model.ViewModel;
using AdminPanel.UI.ActionFilter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminPanel.UI.Controllers
{
    public class DashboardController : Controller
    {
        private readonly IUserService UserService;
        public DashboardController()
        {
            this.UserService = new UserService();
        }
        // GET: Dashboard
        [Auth]
        public ActionResult Index()
        {
            return View();
        }
        [Auth]
        public ActionResult Dashboard()
        {
            return View();
        }

        public ActionResult Login(string returnUrl = "")
        {
            return View(new LoginVM());
        }

        public ActionResult Logout()
        {
            Session.Abandon();
            return Redirect("/Dashboard/Login");
        }

        [HttpPost]
        public ActionResult Login(LoginVM loginVM)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var user = UserService.Login(new Data.Model.UserModel { UserName = loginVM.UserName, Password = loginVM.Password });

            if (user != null && user.Id > 0)
            {
                SessionHelper.User = user;
                return Redirect("~/Product/List");
            }
            else ModelState.AddModelError("Hata","Kullanıcı Adı şifre hatası");

            return View();
        }
    }
}