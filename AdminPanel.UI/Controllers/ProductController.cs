﻿using AdminPanel.Business.Service;
using AdminPanel.Data.Model;
using AdminPanel.Data.Model.ViewModel;
using AdminPanel.UI.ActionFilter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminPanel.UI.Controllers
{
    [Auth]
    public class AsyncProductController : AsyncController
    {
        private readonly IProductService ProductService;

        public AsyncProductController()
        {
            ProductService = new ProductService();
        }


        [HttpPost]
        public JsonResult UpsertProduct(ProductVM model)
        {
            if (model.ProductModel.Id == 0)
                ModelState.Remove("ProductModel.Id");

            if (!ModelState.IsValid)
            {
                return new JsonResult
                {
                    Data = new ResultVM
                    {
                        ErrorMessage = MvcHelper.ResolveModelState(ModelState.Values.ToList()),
                        IsSuccess = false
                    }
                };
            }

            model.ProductModel.CategoryId = model.SelectedProductCategoryId;

            var addResult = this.ProductService.Add(model.ProductModel);

            return new JsonResult
            {
                Data = new ResultVM
                {
                    IsSuccess = addResult.IsSuccess,
                    ErrorMessage = addResult.Errors
                }
            };
        }

    }
    
    [Auth]
    public class ProductController : Controller
    {
        private readonly IProductService ProductService;
        private readonly IProductCategoryService ProductCategoryService;

        public ProductController()
        {
            ProductService = new ProductService();
            ProductCategoryService = new ProductCategoryService();
        }


        [HttpGet]
        public JsonResult GetProductById(long Id)
        {
            ProductVM productVM = new ProductVM();
            productVM.ProductModel = this.ProductService.GetById(Id);

            return Json(productVM, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult GetProductList()
        {
            var result = this.ProductService.GetAll();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult DeleteProduct(long Id)
        {
            var result = this.ProductService.Delete(Id);

            return new JsonResult
            {
                Data = new ResultVM
                {
                    IsSuccess = result,
                    ErrorMessage = result == false ? new List<string> { $"{Id} nolu ürün silinemedi" } : new List<string> { }
                }
            };
        }

        [HttpGet]
        public ActionResult List()
        {

            ProductVM productVM = new ProductVM();
            productVM.ProductCategoryList = this.ProductCategoryService.GetAll();

            return View(productVM);
        }
    }
}