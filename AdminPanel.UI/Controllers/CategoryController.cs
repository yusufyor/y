﻿using AdminPanel.Business.Service;
using AdminPanel.Data.Model.ViewModel;
using AdminPanel.UI.ActionFilter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminPanel.UI.Controllers
{
    [Auth]
    public class AsyncCategoryController : AsyncController
    {
        private readonly IProductCategoryService ProductCategoryService;

        public AsyncCategoryController()
        {
            ProductCategoryService = new ProductCategoryService();
        }


        [HttpPost]
        public JsonResult UpsertCategory(CategoryVM model)
        {
            if (model.ProductCategoryModel.Id == 0)
                ModelState.Remove("ProductCategoryModel.Id");

            ModelState.Remove("SelectedParentProductCategoryId");

            if (!ModelState.IsValid)
            {
                return new JsonResult
                {
                    Data = new ResultVM
                    {
                        ErrorMessage = MvcHelper.ResolveModelState(ModelState.Values.ToList()),
                        IsSuccess = false
                    }
                };
            }

            model.ProductCategoryModel.ParentId = model.SelectedParentProductCategoryId > 0 ? model.SelectedParentProductCategoryId : (long?)null;
            var addResult = this.ProductCategoryService.Add(model.ProductCategoryModel);

            return new JsonResult
            {
                Data = new ResultVM
                {
                    IsSuccess = addResult.IsSuccess,
                    ErrorMessage = addResult.Errors
                }
            };
        }


    }
   
    [Auth]
    public class CategoryController : Controller
    {
        private readonly IProductCategoryService ProductCategoryService;

        public CategoryController()
        {
            ProductCategoryService = new ProductCategoryService();
        }

        // GET: Category
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetCategoryList()
        {
            var result = this.ProductCategoryService.GetAll();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult List()
        {

            CategoryVM categoryVM = new CategoryVM();
            categoryVM.ProductCategoryList = this.ProductCategoryService.GetAll();

            return View(categoryVM);
        }

        [HttpGet]
        public JsonResult GetCategoryById(long Id)
        {
            CategoryVM categoryVM = new CategoryVM();
            categoryVM.ProductCategoryModel = this.ProductCategoryService.GetById(Id);

            return Json(categoryVM, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpsertCategory(CategoryVM model)
        {
            if (model.ProductCategoryModel.Id == 0)
                ModelState.Remove("ProductCategoryModel.Id");

            if (!ModelState.IsValid)
            {
                return new JsonResult
                {
                    Data = new ResultVM
                    {
                        ErrorMessage = MvcHelper.ResolveModelState(ModelState.Values.ToList()),
                        IsSuccess = false
                    }
                };
            }

            model.ProductCategoryModel.ParentId = model.SelectedParentProductCategoryId > 0 ? model.SelectedParentProductCategoryId : (long?)null;
            var addResult = this.ProductCategoryService.Add(model.ProductCategoryModel);

            return new JsonResult
            {
                Data = new ResultVM
                {
                    IsSuccess = addResult.IsSuccess,
                    ErrorMessage = addResult.Errors
                }
            };
        }


        [HttpPost]
        public JsonResult DeleteCategory(long Id)
        {
            var result = this.ProductCategoryService.Delete(Id);

            return new JsonResult
            {
                Data = new ResultVM
                {
                    IsSuccess = result,
                    ErrorMessage = result == false ? new List<string> { $"{Id} nolu category silinemedi" } : new List<string> { }
                }
            };
        }

    }
}