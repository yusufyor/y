﻿using AdminPanel.Business.Helper;
using AdminPanel.Business.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminPanel.UI.ActionFilter
{
    public class AuthAttribute : AuthorizeAttribute
    {
        public AuthAttribute()
        {
           
        }
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (SessionHelper.IsLoggedIn == false)
            {
                filterContext.HttpContext.Response.Redirect("/Dashboard/Login?returnUrl=" + filterContext.HttpContext.Request.RawUrl);
            }
            else { }
        }

    }
}