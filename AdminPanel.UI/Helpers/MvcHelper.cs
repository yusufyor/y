﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AdminPanel.UI
{
    public static class MvcHelper
    {
        public static List<string> ResolveModelState(List<ModelState> modelStates)
        {
            List<string> errors = new List<string>();

            if (modelStates != null && modelStates.Count > 0)
            {
                foreach (var item in modelStates)
                {
                    if (item.Errors != null && item.Errors.Count > 0)
                    {
                        foreach (var itemErrors in item.Errors)
                        {
                            errors.Add(itemErrors.ErrorMessage);
                        }
                    }
                }
            }

            return errors;
        }
    }
}
