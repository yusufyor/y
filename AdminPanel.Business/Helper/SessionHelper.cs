﻿using AdminPanel.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AdminPanel.Business.Helper
{
    public static class SessionHelper
    {
        private static string sessionUserKey = "Session_User";

        public static bool IsLoggedIn
        {
            get
            {
                return User != null && User.Id > 0 ? true : false;
            }
        }

        public static UserModel User
        {
            get
            {
                if (HttpContext.Current == null)
                {
                    return null;
                }
                var session = HttpContext.Current.Session[sessionUserKey];

                if (session != null)
                {
                    return session as UserModel;
                }
                return null;
            }
            set
            {
                HttpContext.Current.Session[sessionUserKey] = value;
            }
        }
    }
}
