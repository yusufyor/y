﻿using AdminPanel.Business.Helper;
using AdminPanel.Data.Attr;
using AdminPanel.Data.Entity;
using AdminPanel.Data.Enum;
using AdminPanel.Data.Model;
using AdminPanel.Data.Model.Util;
using AdminPanel.Data.Model.ViewModel;
using AdminPanel.Data.Repository;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace AdminPanel.Business.Service
{
    public abstract class BaseBusinessService<TModel, TRepository, TEntity> : IBusinessService<TModel, TEntity>
           where TModel : BaseModel, new()
           where TRepository : IRepository<TEntity>, new()
           where TEntity : BaseEntity, new()
    {
        private readonly TRepository repository;
        private readonly ILogRepository logRepository;
        private readonly MapperConfiguration mapperConfig;
        private readonly Mapper mapper;

        public BaseBusinessService()
        {
            repository = new TRepository();
            logRepository = new LogRepository();

            mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ProductEntity, ProductModel>().ReverseMap();
                cfg.CreateMap<ProductCategoryEntity, ProductCategoryModel>();
                cfg.CreateMap<ProductCategoryModel, ProductCategoryEntity>().ForMember(x => x.Media, src => src.Ignore());
                cfg.CreateMap<MediaEntity, MediaModel>().ReverseMap();
                cfg.CreateMap<UserEntity, UserModel>().ReverseMap();

                cfg.CreateMap<LogEntity, LogModel>().ReverseMap();
            });


            mapper = new Mapper(mapperConfig);
        }

        public virtual Result Add(TModel model)
        {
            TEntity entity = new TEntity();

            try
            {
                entity = mapper.Map<TEntity>(model);


                if (model.Id > 0)
                {
                    var oldData = GetById(model.Id);

                    var result = repository.Update(entity) ?
                        new Result { IsSuccess = true } : new Result { IsSuccess = false };

                    if (result.IsSuccess)
                        AddLog(model, oldData);

                    return result;
                }
                else
                {
                    bool result = repository.Insert(entity);
                    if (result)
                    {
                        model.Id = entity.Id;

                        return new Result { IsSuccess = true };
                    }
                    else
                        return new Result { IsSuccess = false };
                }
            }
            catch (Exception ex)
            {
                return new Result
                {
                    Errors = new List<string> { ex.ToString() },
                    IsSuccess = false
                };
            }
        }

        public virtual bool Delete(long Id)
        {
            return repository.Delete(Id);
        }

        public bool Delete(TModel entityToDelete)
        {
            TEntity entity = mapper.Map<TEntity>(entityToDelete);

            return repository.Delete(entity);
        }

        public List<TModel> Get(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null)
        {
            var result = repository.Get(filter: filter, orderBy: orderBy);

            List<TModel> modelList = new List<TModel>();

            try
            {
                modelList = mapper.Map<List<TModel>>(result);
            }
            catch (Exception ex)
            {

            }
            return modelList;

        }

        public virtual List<TModel> GetAll(string includeProperties = "")
        {
            var result = repository.Get(x => x.Status != (int)StatusType.Deleted, includeProperties: includeProperties);
            List<TModel> modelList = new List<TModel>();

            try
            {
                modelList = mapper.Map<List<TModel>>(result);
            }
            catch (Exception ex)
            {

            }
            return modelList;
        }

        public virtual TModel GetById(long Id, string includeProperties = "")
        {
            TModel model = new TModel();

            var result = repository.GetById(Id, includeProperties);

            try
            {
                model = mapper.Map<TModel>(result);
            }
            catch (Exception ex) { }

            return model;

        }

        public bool Update(TModel model)
        {
            TEntity entity = mapper.Map<TEntity>(model);

            return repository.Update(entity);
        }

        private void AddLog(TModel model, TModel oldData)
        {
            if (model.GetType().GetCustomAttributes(false).Any(a => a.GetType() == typeof(TableAttribute)))
            {

                List<LogDescription> logDescriptions = new List<LogDescription>();

                foreach (var item in model.GetType().GetProperties().Where(x => x.GetCustomAttributes(false).Any(a => a.GetType() == typeof(LogAttribute))))
                {
                    var newValue = item.GetValue(model);
                    var oldValue = oldData.GetType().GetProperty(item.Name).GetValue(oldData);


                    if (newValue != oldValue)
                    {
                        logDescriptions.Add(new LogDescription
                        {
                            ColumnName = item.Name,
                            NewValue = newValue != null ? newValue.ToString() : "",
                            OldValue = oldValue != null ? oldValue.ToString() : ""
                        });
                    }
                }

                if (logDescriptions.Count > 0)
                {
                    string _description = new JavaScriptSerializer().Serialize(logDescriptions);

                    logRepository.Insert(new LogEntity
                    {
                        UserId = SessionHelper.User != null ? SessionHelper.User.Id : (long?)null,
                        PkId = model.Id,
                        Status = (int)StatusType.Active,
                        Date = DateTime.Now,
                        TableName = (model.GetType().GetCustomAttributes(false).FirstOrDefault(a => a.GetType() == typeof(TableAttribute)) as TableAttribute).Name,
                        Description = _description
                    });
                }
            }
        }
    }

}
