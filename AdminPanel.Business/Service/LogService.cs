﻿using AdminPanel.Data.Entity;
using AdminPanel.Data.Model;
using AdminPanel.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminPanel.Business.Service
{
    public interface ILogService : IBusinessService<LogModel, LogEntity>
    {

    }

    public class LogService : BaseBusinessService<LogModel, LogRepository, LogEntity>, ILogService
    {
        public LogService()
        {
        }   
    }
}
