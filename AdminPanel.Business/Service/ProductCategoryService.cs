﻿using AdminPanel.Data.Entity;
using AdminPanel.Data.Model;
using AdminPanel.Data.Model.ViewModel;
using AdminPanel.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminPanel.Business.Service
{
    public interface IProductCategoryService : IBusinessService<ProductCategoryModel, ProductCategoryEntity>
    { }

    public class ProductCategoryService : BaseBusinessService<ProductCategoryModel, ProductCategoryRepository, ProductCategoryEntity>, IProductCategoryService
    {
        private readonly IMediaService MediaService;
        public ProductCategoryService()
        {
            this.MediaService = new MediaService();
        }

        public override bool Delete(long Id)
        {
            var data = base.GetById(Id);

            if (data != null && data.MediaId != null && data.MediaId > 0)
            {
                MediaService.Delete((long)data.MediaId);
            }

            return base.Delete(Id);
        }

        public override ProductCategoryModel GetById(long Id, string includeProperties = "")
        {
            return base.GetById(Id, includeProperties: "Media,ParentProductCategory");
        }

        public override List<ProductCategoryModel> GetAll(string includeProperties = "")
        {
            return base.GetAll(includeProperties: "Media,ParentProductCategory");
        }

        public override Result Add(ProductCategoryModel model)
        {
            Result result = new Result();

            result = base.Add(model);

            if (result.IsSuccess && model.Media != null && model.Media.HttpPostedFileBase != null)
            {
                var mediaResult = MediaService.Add(model.Media);
                if (mediaResult.IsSuccess)
                {
                    model.MediaId = model.Media.Id;
                    base.Add(model);
                }
            }

            return result;
        }
    }
}
