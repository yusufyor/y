﻿using AdminPanel.Data.Entity;
using AdminPanel.Data.Model;
using AdminPanel.Data.Model.ViewModel;
using AdminPanel.Data.Repository;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AdminPanel.Business.Service
{
    public interface IProductService : IBusinessService<ProductModel, ProductEntity>
    {

    }

    public class ProductService : BaseBusinessService<ProductModel, ProductRepository, ProductEntity>, IProductService
    {

        private readonly IProductCategoryService ProductCategoryService;

        public ProductService()
        {
            ProductCategoryService = new ProductCategoryService();
        }

        public override List<ProductModel> GetAll(string includeProperties = "")
        {
            return base.GetAll("Category");
        }

        public override ProductModel GetById(long Id, string includeProperties = "")
        {
            return base.GetById(Id, "Category");
        }


        public override Result Add(ProductModel model)
        {
            Result result = new Result();

            var categoryResult = this.ProductCategoryService.GetById(model.CategoryId);

            if (categoryResult == null || categoryResult.Id == 0)
            {
                result.IsSuccess = false;
                result.Errors.Add("CategoryId hatalı");
            }

            if (result.IsSuccess == false)
                return result;
            else result.IsSuccess = true;

            return base.Add(model);
        }
    }
}
