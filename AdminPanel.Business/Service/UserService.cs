﻿using AdminPanel.Business.Helper;
using AdminPanel.Data.Entity;
using AdminPanel.Data.Enum;
using AdminPanel.Data.Model;
using AdminPanel.Data.Model.ViewModel;
using AdminPanel.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminPanel.Business.Service
{
    public interface IUserService : IBusinessService<UserModel, UserEntity>
    {
        UserModel Login(UserModel userModel);
    }

    public class UserService : BaseBusinessService<UserModel, UserRepository, UserEntity>, IUserService
    {
        public UserService()
        {

        }

        public override Result Add(UserModel model)
        {
            model.Password = CreateMD5(model.Password);
            return base.Add(model);
        }

        public UserModel Login(UserModel userModel)
        {
            string hashPass = CreateMD5(userModel.Password).ToLower();

            var currentUser = base.Get(x => x.UserName == userModel.UserName && x.Password == hashPass && x.Status == (int)StatusType.Active).FirstOrDefault();

            return currentUser;
        }

        private string CreateMD5(string input)
        {
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }
    }
}
