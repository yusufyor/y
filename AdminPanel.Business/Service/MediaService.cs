﻿using AdminPanel.Data.Entity;
using AdminPanel.Data.Model;
using AdminPanel.Data.Model.ViewModel;
using AdminPanel.Data.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AdminPanel.Business.Service
{
    public interface IMediaService : IBusinessService<MediaModel, MediaEntity>
    {

    }

    public class MediaService : BaseBusinessService<MediaModel, MediaRepository, MediaEntity>, IMediaService
    {
        public MediaService()
        {

        }

        public override bool Delete(long Id)
        {
            var data = base.GetById(Id);

            if (data != null && !string.IsNullOrEmpty(data.Link))
            {
                try
                {
                    string path = System.Web.HttpContext.Current.Server.MapPath(data.Link);

                    bool exists = System.IO.File.Exists(path);
                    if (exists)
                        System.IO.File.Delete(path);
                }
                catch { }
            }

            return base.Delete(Id);
        }

        public override Result Add(MediaModel model)
        {
            Result result = new Result();

            if (model.HttpPostedFileBase == null || model.HttpPostedFileBase.ContentLength < 1)
            {
                result.IsSuccess = false;
                result.Errors.Add("Dosya bulunamadı");
            }

            string extension = Path.GetExtension(model.HttpPostedFileBase.FileName).ToLower().TrimStart('.');

            if (extension == "jpg" || extension == "jpeg" || extension == "png")
            { }
            else
            {
                result.IsSuccess = false;
                result.Errors.Add("Dosya formatı yanlış. jpg veya png olabilir");
            }

            var addResult = base.Add(model);

            if (addResult.IsSuccess)
            {
                try
                {
                    string orgPath = $"/Uploads/Media/{model.Id.ToString()}/{model.HttpPostedFileBase.FileName}";

                    string path = System.Web.HttpContext.Current.Server.MapPath($"~/Uploads/Media/{model.Id.ToString()}");

                    bool exists = System.IO.Directory.Exists(path);
                    if (!exists)
                        System.IO.Directory.CreateDirectory(path);


                    var media = base.GetById(model.Id);

                    model.HttpPostedFileBase.SaveAs(path + "\\" + model.HttpPostedFileBase.FileName);
                    media.Link = orgPath;
                    base.Add(media);
                }
                catch (Exception ex) { }
            }

            return result;
        }
    }
}
