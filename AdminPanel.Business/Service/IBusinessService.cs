﻿using AdminPanel.Data.Entity;
using AdminPanel.Data.Model;
using AdminPanel.Data.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AdminPanel.Business.Service
{
    public interface IBusinessService<TModel, TEntity> where TModel : BaseModel where TEntity : BaseEntity
    {
        List<TModel> GetAll(string includeProperties = "");
        Result Add(TModel model);
        bool Update(TModel model);
        bool Delete(long Id);
        bool Delete(TModel entityToDelete);
        TModel GetById(long Id, string includeProperties = "");
        List<TModel> Get(
         Expression<Func<TEntity, bool>> filter = null,
         Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null);
    }
}
